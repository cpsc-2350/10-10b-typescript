export default interface Point {
  readonly x: number;
  readonly y: number;
  readonly magnitude: number;

  equals(o: { x: number; y: number }): boolean;
  //  getMagnitude(): number;
  distance(o: { x: number; y: number }): number;
  move(delta: { x: number; y: number }): Point;
  move(dx: number, dy: number): Point;
}

export class PointImpl implements Point {
  // implementation of the interface Point
  readonly magnitude: number; // all variables must be initialized by the end of the constructor

  constructor(readonly x: number, readonly y: number) {
    this.magnitude = this.distance({ x: 0, y: 0 });
  }

  equals(o: { x: number; y: number }): boolean {
    return o.x === this.x && o.y == this.y;
  }

  /*
  get magnitude(): number {
    return this.magnitude;
    
    //return this.distance({ x: 0, y: 0 });
    //const foo = { x: 0, y: 0 , z: 0}; // typscript sctuctural subtyping 
    //return this.distance(foo);        // any parameter that provides a x and y, as numbers, will be acepted
    
  }
  */

  distance(o: { x: number; y: number }): number {
    return Math.sqrt((this.x - o.x) ** 2 + (this.y - o.y) ** 2);
  }

  move(delta: { x: number; y: number }): PointImpl;
  move(dx: number, dy: number): PointImpl;
  move(d1: number | { x: number; y: number }, d2?: any): PointImpl {
    // the | says the first parameter cand be a number or a { x: number; y: number }, and ? says that the second parameter, IF EXIST, will be any type
    if (typeof d1 === "number") {
      return new PointImpl(this.x + d1, this.y + d2); //canNOT instantiate an interface, like "new Point"
    } else {
      return new PointImpl(this.x + d1.x, this.y + d1.y);
    }
  }
}
