import Point from "./Point";
import Dimension from "./Dimension";

export default interface Retctangle {
  readonly position: Point; //declaring a variable as readonly
  readonly size: Dimension;
}
