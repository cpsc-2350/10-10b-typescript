import Point, { PointImpl } from "./Point";

export default interface Line {
  readonly length: number;

  add(p: Point): void;
  add(x: number, y: number): void;
  contains(p: { x: number; y: number }): boolean;
}

export class LineImpl implements Line {
  private readonly points: Point[] = [];

  get length(): number {
    let len = 0;
    for (let i = 1; i < this.points.length; i++) {
      len += this.points[i].distance(this.points[i - 1]);
    }
    return len;
    //this.points.reduce((pv, cv, i, arr) => , 0)
  }

  add(p: Point): void;
  add(x: number, y: number): void;
  add(arg1: Point | number, arg2?: any) {
    this.points.push(
      typeof arg1 === "number" ? new PointImpl(arg1, arg2) : arg1
    );
  }
  contains(p: { x: number; y: number }): boolean {
    return this.points.find((o) => o.equals(p)) != undefined;
  }
}
