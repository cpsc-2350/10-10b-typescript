export default interface Dimension {
  readonly w: number;
  readonly h: number;
}
