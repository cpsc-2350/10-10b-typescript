import Point, { PointImpl } from "./Point"; //destructuring assignment to import PointImpl as an object, because it was exported as "default", without a name
import { LineImpl } from "./Line";

export default function () {
  /*
  const points = [];

  for (let i = 0; i < 10; i++) {
    points[i] = i % 2 === 0 ? new PointImpl(i, -i) : new PointImpl(-i, i);
  }
  console.log(points[0].distance(points[3]));
  console.log(points[1].magnitude);
  */
  const line = new LineImpl();
  //line.add(new PointImpl(1, 1));
  line.add(1, 1);
  console.log("0", line.length);
  //line.add(new PointImpl(0, 0));
  line.add(0, 0);
  console.log("1", line.length);
  console.log(line.contains({ x: 1, y: 1 }));
  console.log(line.contains({ x: 0, y: 1 }));
}
